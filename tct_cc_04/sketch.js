// (138,43,226)
// (230,230,250)
let drops;

function setup() {
	createCanvas(640, 360);
	drops = Array(400).fill().map(a => new Drop());
}

function draw() {
	background(230,230,250);
	for ( let d of drops){
		d.fall();
		d.show();
	}
}

class Drop{
	constructor(){
		this.x = random(0,width);
		this.y = random(0,-400);
		this.z = random(0,20);
		this.length = map(this.z,0,20,10,20);
		this.yspeed = map(this.z,0,20,2,5);
		this.thick = map(this.z,0,20,0,2);
	}

	fall(){
		this.y = this.y + this.yspeed;
		let gravity = map(this.z,0,20,0.01,0.05);
		this.yspeed += gravity;
		if ( this.y > height){
			this.y = random(0,-200);
			this.yspeed = map(this.z,0,20,2,5);
		}
	}

	show(){
		strokeWeight(this.thick)
		stroke(138,43,226);
		line(this.x,this.y,this.x,this.y+this.length);
	}
}