let cols, rows;
let w = 20;
let grid = [];
let current = [];
let stack = [];

function setup() {
	createCanvas(800, 600);
	cols = floor(width / w);
	rows = floor(height / w);

	frameRate(30)

	for (let r = 0; r < rows; r++) {
		for (let c = 0; c < cols; c++) {
			let cell = new Cell(c, r);
			grid.push(cell);
		}
	}

	current = grid[0];
}

function draw() {
	background(51);
	
	for (let cell of grid) cell.show();
	current.visited = true;
	current.highlight();

	// STEP 1
	let next = current.checkNeighbors();
	if (next) {
		next.visited = true;

		// STEP 2
		stack.push(current);

		// STEP 3
		removeWalls(current,next);

		// STEP 4
		current = next;
	} 
	else if (stack.length) {
		current = stack.pop();;
	}
}

class Cell {
	constructor(c, r) {
		this.c = c;
		this.r = r;
		this.walls = [true, true, true, true];
		this.visited = false;
	}

	checkNeighbors() {
		let neighbors = [];

		let top = grid[index(this.c, this.r - 1)];
		let right = grid[index(this.c + 1, this.r)];
		let bottom = grid[index(this.c, this.r + 1)];
		let left = grid[index(this.c - 1, this.r)];

		top && !top.visited && neighbors.push(top);
		right && !right.visited && neighbors.push(right);
		bottom && !bottom.visited && neighbors.push(bottom);
		left && !left.visited && neighbors.push(left);

		if (neighbors.length > 0) {
			let r = floor(random(0, neighbors.length));
			return neighbors[r];
		} else {
			return undefined;
		}
	}

	show() {
		let x = this.c * w;
		let y = this.r * w;

		if (this.visited) {
			noStroke();
			fill(150, 30, 150)
			rect(x, y, w, w);
		}

		fill(0);
		stroke(255);

		if (this.walls[0]) line(x, y, x + w, y)
		if (this.walls[1]) line(x + w, y, x + w, y + w)
		if (this.walls[2]) line(x + w, y + w, x, y + w)
		if (this.walls[3]) line(x, y + w, x, y)

	}

	highlight(){
		let x = this.c * w;
		let y = this.r * w;
		noStroke();
		fill(190, 10, 190)
		rect(x+1, y+1, w-2, w-2);
	}
}

function index(c, r) {
	if (c < 0 || r < 0 || c > cols - 1 || r > rows - 1) return -1;
	return c + r * cols;
}

function removeWalls(a,b){
	let x = a.c - b.c;
	if( x === 1 ){
		a.walls[3] = false;
		b.walls[1] = false;
	} else if( x === -1){
		a.walls[1] = false;
		b.walls[3] = false;
	}

	let y = a.r - b.r;
	if( y === 1 ){
		a.walls[0] = false;
		b.walls[2] = false;
	} else if( y === -1){
		a.walls[2] = false;
		b.walls[0] = false;
	}
}