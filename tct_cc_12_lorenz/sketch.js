let x = 0.01;
let y = 0;
let z = 0;

const a = 10;
const b = 28;
const c = 8/3;

let points = new Array();

function setup() {
	createCanvas(800, 600, WEBGL);
	colorMode(HSB);
}

function draw() {

	background(0);

	let dt = 0.01;

	let dx = (a * ( y - x ) ) * dt;
	let dy = (x * ( b - z ) - y) * dt;
	let dz = (x * y - c * z ) * dt ;

	x += dx;
	y += dy;
	z += dz;

	points.push(new p5.Vector(x, y, z));

	translate(0, 0, -80);
	
	let camX = map(mouseX, 0, width, -400, 400);
	let camY = map(mouseY, 0, height, -400,400); 
	camera(camX, camY, (height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, 1, 0);

	noFill();
	scale(5);

	let hu = 0;

	beginShape();
	for (let v of points) {
		// let offset = new p5.Vector(random(-1,1), random(-1,1), random(-1,1));
		// offset.mult(0.01);
		// v.add(offset);

		stroke(hu, 255, 255);
		vertex(v.x, v.y, v.z);

		hu += 1;
		if (hu > 255) {
			hu = 0;
		}
	}
	endShape();

}