// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain
// Code for: https://youtu.be/17WoOqgXsRM

var stars = [];

var speed;

function setup() {
	createCanvas(600, 600);
	for (var i = 0; i < 400; i++) {
		stars[i] = new Star();
	}
	frameRate(30);
}

function draw() {
	speed = 10;
	background(0);
	translate(width / 2, height / 2);
	for (var i = 0; i < stars.length; i++) {
		stars[i].update();
		stars[i].show();
	}
}

function Star() {
	this.x = random(-width / 2, width / 2);
	this.y = random(-height / 2, height / 2);
	this.z = random(width);
	this.pz = this.z;

	this.update = function () {
		this.z = this.z - speed;
		if (this.z < 1) {
			this.z = width;
			this.x = random(-width, width);
			this.y = random(-height, height);
			this.pz = this.z;
		}
	}

	this.show = function () {
		fill(255);
		noStroke();

		var sx = map(this.x / this.z, 0, 3, 0, width);
		var sy = map(this.y / this.z, 0, 3, 0, height);

		var r = map(this.z, 0, width, 3, 0);
		ellipse(sx, sy, r, r);

		var px = map(this.x / this.pz, 0, 3, 0, width);
		var py = map(this.y / this.pz, 0, 3, 0, height);

		this.pz = this.z;

		stroke(255);
		line(px, py, sx, sy);

	}
}