let dots = [];
let cols = 40;
let rows = 10;
let gridWidth;
let gridHeight;

function setup() {
	createCanvas(800, 600);
	gridWidth = width/cols;
	gridHeight = height/rows*1.5;
	frameRate(30)

	for(let r = rows; r>0; r--){
		for(let c = -cols/2; c<cols/2; c++){
			let z = r*gridHeight;
			let x = c*gridWidth;
			dots.push( new Dot(x,random(90,110),z));
		}
	}
}

function draw() {
	background(0);
	translate(width / 2, 0);

	for( let dot of dots) dot.update();
	for( let dot of dots) {
		let coordinates = getCoordinates(dots.indexOf(dot));
		dot.show(coordinates);
	}
}

class Dot{
	constructor(x,y,z){
		this.x = x;
		this.y = y;
		this.z = z;
		this.zs = -10;
		this.r = 2;
	}

	show(coordinates){
		stroke(120,30,180);

		let x = map(this.x/this.z,0,1,0,width);
		let y = map(this.y/this.z,0,1,0,height);

		// ellipse(x,y,this.r,this.r);

		for( let c of coordinates){
			let xl = map(c.x/c.z,0,1,0,width);
			let yl = map(c.y/c.z,0,1,0,height);
			line(x,y,xl,yl);
		}
	}

	update(){
		this.z += this.zs;
		if(this.z < 0){
			this.z = height*1.5;
			this.y = random(90,110);
		}
	}

}

function getCoordinates(i){
	let indexes = [];

	if ((i+1)%cols !== 0) indexes.push(i+1);
	
	if (dots[i].z>gridHeight && (i+1) < cols*(rows-1)){
		indexes.push(i+cols);
	} else if(dots[i].z>gridHeight && i >= cols*rows-cols ){
		let index = map(i,cols*(rows-1),cols*rows,0,cols)
		indexes.push(index);
	}

	let coordinates = [];
	for( let index of indexes){
		let d = dots[index];
		coordinates.push({x:d.x,y:d.y,z:d.z});
	}

	return coordinates;
}