let particles = [];
let weather;

function setup(){
  createCanvas(600,400);
  frameRate(28);
  weather = new Weather();
}

function draw(){0
  background(0);
  let r = Math.floor(Math.random() * 5);
  let newParticles = Array(r).fill().map(a=>new Particle());
  particles = [...particles,...newParticles];
  particles.sort((a,b)=>a.col-b.col);
  weather.update();

  for( let particle of particles){
    particle.update(weather);
    particle.show();
  }
  particles = particles.filter(p=>!p.finished());
  
//   let sumV = particles.reduce((v,p)=>v.add(p.x,p.y), createVector(0,0)).div(particles.length);                           
//   fill(255,0,0);
//   ellipse(sumV.x,sumV.y,24,24);

}

class Particle {
  constructor(){
    this.x = 300;
    this.y = 380;
    this.vx = random(-5,5);
    this.vy = random(-5,-1);
    this.col = random(256);
    this.alpha = 255;
  }
  
  finished(){
    return this.alpha < 0;
  }
  
  update(weather){
	this.vx = this.vx/1.05 + random(-0.3,0.3) + weather.returnWind();
    this.x += this.vx;
    this.y += this.vy;
    this.alpha -= 3.5;
  }
  
  show(){
	stroke(255,0);
    fill(this.col,this.alpha);
	ellipse(this.x,this.y,16);
  }

}

class Weather{
	constructor(){
		this.wind = random(-1.2,1.2);
		this.windAcc = random(-0.1,0.1);
	}

	update(){
		this.windAcc = random(-0.1,0.1);
		this.wind = this.wind + this.windAcc;
	}

	returnWind(){
		return this.wind;
	}
}
