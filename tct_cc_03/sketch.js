let s;
let scl = 25;
let fr = 10;
let food;

function setup() {
	createCanvas(500, 500);
	s = new Snake();
	frameRate(fr);
	pickLocation();
}

function pickLocation() {
	let cols = floor(width / scl);
	let rows = floor(height / scl);

	let fx = floor(random(cols));
	let fy = floor(random(rows));
	food = createVector(fx, fy);
	food.mult(scl);
}

function draw() {
	background(30);

	s.death();
	if(s.gameOver){
		fill(254);
		textAlign(CENTER);
		textSize(20)
		text('GAME OVER :(', width/2,height/2-scl);
		text('PRESS ENTER TO PLAY AGAIN', width/2,height/2+scl);
	} else{
		s.update();
		s.show();
	}

	if (s.eat(food)) {
		pickLocation();
	}

	fill(255, 0, 100);
	rect(food.x, food.y, scl, scl);

	fill(254,200);
	textAlign(RIGHT);
	textSize(20)
	text('SCORE : ' + s.total , width-2*scl,scl*2);
	text('HIGH SCORE : ' + s.highScore , width-2*scl,scl*4);
}

function Snake() {
	this.x = 0;
	this.y = 0;
	this.xs = 1;
	this.ys = 0;
	this.xs_next = this.xs;
	this.ys_next = this.ys;
	this.total = 0;
	this.tail = [];
	this.gameOver = false;
	this.highScore = 0;

	this.eat = function (pos) {
		var d = dist(this.x, this.y, pos.x, pos.y);
		if (d < 1) {
			s.total++;
			// fr++;
			// frameRate(fr);
			return true;
		} else {
			return false;
		}
	}

	this.dir = function (x, y) {
		if ((this.xs + x)!==0 && (this.ys + y)!==0) {
			this.xs_next = x;
			this.ys_next = y;
		}
	}

	this.death = function () {
		for (let piece of this.tail) {
			let d = dist(this.x, this.y, piece.x, piece.y);
			if (d < 1) {
				if (this.total > this.highScore) this.highScore = this.total;
				this.total = 0;
				this.tail = [];
				this.gameOver = true;
			}
		}
	}

	this.update = function () {
		if (this.tail.length === this.total) {
			for (let i = 0; i < this.total - 1; i++) {
				this.tail[i] = this.tail[i + 1];
			}
		}
		this.tail[this.total - 1] = createVector(this.x, this.y)

		this.xs = this.xs_next;
		this.ys = this.ys_next;

		if((this.x + this.xs * scl)>(width - scl)){
			this.x = 0;
		} else if((this.x + this.xs * scl)<0) {
			this.x = width - scl;
		} else {
			this.x = this.x + this.xs * scl;
		}

		if((this.y + this.ys * scl)>(height - scl)){
			this.y = 0;
		} else if((this.y + this.ys * scl)<0) {
			this.y = height - scl;
		} else {
			this.y = this.y + this.ys * scl;
		}
	}

	this.show = function () {
		fill(255);
		for (let i = 0; i < this.total; i++) {
			rect(this.tail[i].x, this.tail[i].y, scl, scl);
		}
		rect(this.x, this.y, scl, scl);
	}
}

function keyPressed() {
	switch (keyCode) {
		case UP_ARROW:
			s.dir(0, -1);
			break;
		case RIGHT_ARROW:
			s.dir(1, 0);
			break;
		case DOWN_ARROW:
			s.dir(0, 1);
			break;
		case LEFT_ARROW:
			s.dir(-1, 0);
	}

	if(keyCode === ENTER){
		s.gameOver = false;
	}
}